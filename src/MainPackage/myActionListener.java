package MainPackage;

import java.awt.Color;
import java.awt.event.*;
import javax.swing.*;

public class myActionListener extends MouseAdapter {

    private final Start mainFrame;

    public myActionListener(Start mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        Field actualButton = (Field) e.getSource(); //aktualnie wcisciety button

        //wystartuj timer
        if (mainFrame.timer == null || !mainFrame.timer.isRunning()) {
            startTimer();
        }

        //lewy
        if (e.getButton() == MouseEvent.BUTTON1) {

            if (actualButton.isEnabled() && !actualButton.flag) {
                actualButton.setEnabled(false);

                if (actualButton.state == 1) { //jeśli click na mine
                    mainFrame.timer.stop();
                    showAllMines(); //pokaz wszystkie miny
                    //chooser zaczecia od nowa albo zamkniecia gry
                    Object[] options = {"Zacznij od nowa", "Zakończ"};
                    int choose = JOptionPane.showOptionDialog(null, "Wdepnąłeś :( \n", "Przegrałeś!",
                            JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE,
                            null, options, options[1]);

                    if (choose == 0) { //zacznij od nowa
                        mainFrame.dispose();
                        new Start();
                    } else { //zakończ
                        mainFrame.dispose();
                    }
                } else { //jeśli click na zwykłe pole
                    checkWin(); //sprawdz czy uzytkownik rozbroil wszystkie miny

                    if (actualButton.state == 0) {
                        deactivateEmptyNeighbors(actualButton); //dezaktywuj sasiadujace pola ktore sa puste (nie maja sasiadow min)   
                    }
                }
            }
        }

        //prawy
        if (e.getButton() == MouseEvent.BUTTON3) {

            if (!actualButton.flag) { //jezeli dane pole nie jest oflagowane -> to oflaguj
                actualButton.setBackground(Color.red);
                actualButton.setText("M");
                actualButton.flag = true;
                mainFrame.flags++;
                if (actualButton.state == 1) {
                    this.mainFrame.correctFlags++; //jeżeli mina została oflagowana
                }
                checkWin(); //sprawdz czy uzytkownik rozbroil wszystkie miny
            } else { //jezeli dane pole jest oflagowane -> to odflaguj
                actualButton.setBackground(null);
                actualButton.flag = false;
                actualButton.setText("");
                mainFrame.flags--;
                if (actualButton.state == 1) {
                    this.mainFrame.correctFlags--; //jeżeli mina została odflagowana
                }
            }
            mainFrame.jLabelMineCounterContent.setText(Short.toString(mainFrame.flags));
        }
    }

    //sprawdz czy uzytkownik rozbroil wszystkie miny
    private void checkWin() {

        //sprawdz czy wszystkie miny zostały znalezione
        if (mainFrame.flags == mainFrame.mineCounter && mainFrame.correctFlags != mainFrame.mineCounter) {

            JOptionPane.showMessageDialog(null,
                    "Nie wszystkie miny zostały znalezione!",
                    "Coś poszło nie tak :(",
                    JOptionPane.WARNING_MESSAGE);

        } else //jezeli wszystkie miny zostaly oflagowane
        if (mainFrame.flags == mainFrame.mineCounter && mainFrame.correctFlags == mainFrame.mineCounter) {
            mainFrame.timer.stop();

            Object[] options = {"Zacznij od nowa", "Zakończ"};
            int choose = JOptionPane.showOptionDialog(null,
                    "Gratulacje! \n Rozbroiłeś/aś pole minowe \n Twój czas to: " + mainFrame.jLabelTimerContent.getText(),
                    "Wygarłeś/aś!",
                    JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE,
                    null, options, options[1]);

            if (choose == 0) { //zacznij od nowa
                mainFrame.dispose();
                new Start();
            } else { //zakończ
                mainFrame.dispose();
            }
        }
    }

    private void startTimer() {
        mainFrame.timer = new Timer(1000, new ActionListener() {
            int time = 0;

            @Override
            public void actionPerformed(ActionEvent e) {
                time++;
                mainFrame.jLabelTimerContent.setText(format(time / 60) + ":" + format(time % 60));
            }
        });
        mainFrame.timer.start();
    }

    //funkcja pomocnicza do formatu do czasu
    private static String format(int i) {
        String result = String.valueOf(i);
        if (result.length() == 1) {
            result = "0" + result;
        }
        return result;
    }

    //sprawdzenie sasiadow - dezaktywacja pol ktore maja 0 min-sasiadow obok siebie
    private void deactivateEmptyNeighbors(Field actualButton) {

        //ustawienie pola ktorego chcemy spawdzic sasiadow
        int posX = actualButton.x;
        int posY = actualButton.y;

        //jezeli aktualne pole ma 0 sasiadow-min
        if (actualButton.neighbors == 0) {

            //przesukiwanie sasiadow kazdego pola
            for (int row = posX - 1; row <= posX + 1; row++) {
                for (int col = posY - 1; col <= posY + 1; col++) {

                    if ((row == posX && col == posY) || row < 0 || col < 0 || row >= mainFrame.x || col >= mainFrame.y) {
                        continue;
                    }

                    //jezeli pole ma 0 sasiadow-min
                    if (mainFrame.FieldsArray[row][col].neighbors == 0) {

                        //jezel nie jest mina i jest dostepne mina lub jest disabled idz dalej
                        if (mainFrame.FieldsArray[row][col].state == 0 && mainFrame.FieldsArray[row][col].isEnabled()) {
                            mainFrame.FieldsArray[row][col].setEnabled(false);
                            mainFrame.FieldsArray[row][col].setText("");
                            deactivateEmptyNeighbors(mainFrame.FieldsArray[row][col]);
                        }
                    } else { //jezeli pole ma != 0 sasiadow-min
                        mainFrame.FieldsArray[row][col].setEnabled(false);
                        mainFrame.FieldsArray[row][col].setText(Short.toString(mainFrame.FieldsArray[row][col].neighbors));
                    }
                }
            }
        } else {
            actualButton.setEnabled(false);
            actualButton.setText(Short.toString(actualButton.neighbors));

        }

    }

    //kiedy uzytkownik kliknie na mine to pokazuje rozmieszczenie wszystkich min
    private void showAllMines() {
        for (int row = 0; row < mainFrame.x; row++) {
            for (int col = 0; col < mainFrame.y; col++) {
                if (mainFrame.FieldsArray[row][col].state == 1) {
                    mainFrame.FieldsArray[row][col].setText("M");
                    mainFrame.FieldsArray[row][col].setBackground(Color.red);
                } else if (mainFrame.FieldsArray[row][col].state == 0 && mainFrame.FieldsArray[row][col].flag == true){
                    mainFrame.FieldsArray[row][col].setText("X");
                    mainFrame.FieldsArray[row][col].setBackground(Color.BLUE);
                }
            }
        }
    }
    
}

package MainPackage;

import javax.swing.JButton;

/**
 * @author Andrie
 */
public class Field extends JButton {

    /**
     * ID - wykorzysytwane do losowego wyznaczania min
     */
    public Short ID;

    /**
     * stany danego pola: 0 - pusty 1 - mina
     */
    public short state;

    /**
     * czy dane pole jest oflagowane przez uzytkownika jesli tak, jest
     * zablokowane
     */
    public boolean flag;
   
    /**
     * położenie danego pola (buttonu)
     */
    public short x, y; 
    
    /**
     * liczba min w sasiedztwie pola
     */
    public short neighbors;
    
    public Field(short x, short y, int ID) {

        this.x = x;
        this.y = y;
        this.state = 0;
        this.ID = (short) ID;
        this.flag = false;
        this.neighbors = 0;
    }
    
}
